package entities;


import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * @author Borimir Georgiev
 */
public class Menu
{
    private static final String MSGERR = "Please input a correct name..";
    private static final String INPUT_MSG = "Please input person name: ";
    BirthdayManager manager;
    private Scanner sc;

    public Menu()
    {
        this.manager = new BirthdayManager();
        this.sc = new Scanner(System.in);
    }

    /**
     * Prints menu, reads user choice and acts accordingly.
     */
    public void printMenu()
    {
        System.out.println("Please choose an option:");
        System.out.println("[1] Add birthday");
        System.out.println("[2] Countdown until birthday");
        System.out.println("[3] Get age of a person");
        System.out.println("[4] Get soonest birthday");

        int userChoice = read();
        proceed(userChoice);
    }

    private int read()
    {
        int userChoice = 0;

        // Checks if it's a number.
        try
        {
            userChoice = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e)
        {
            System.out.println("Invalid choice! Input must be a number.");
            printMenu();
            read();
        }

        // Checks if the list of options contains a number same as the users num.
        if (userChoice < 6)
        {
            return userChoice;

        } else
        {
            System.out.println("There isn't a choice with such number.");
            System.out.println("Please input a number between 1 and 5");
            printMenu();
            return read();
        }
    }

    public void proceed(int userChoice)
    {
        switch (userChoice)
        {
            case 1:
                addOption();
                printMenu();
                break;
            case 2:
                countdownOption();
                printMenu();
                break;
            case 3:
                getAgeOption();
                printMenu();
                break;
            case 4:
                manager.getSoonestBirthday();
                printMenu();
                break;
        }
    }

    private void getAgeOption()
    {
        System.out.println(INPUT_MSG);
        String name = sc.nextLine();


        manager.getBirthdays().forEach((k, v) -> {
            if (v.contains(name))
            {
                System.out.println(k.getAge());
            }
        });
    }

    private void countdownOption()
    {
        System.out.println(INPUT_MSG);
        String name = sc.nextLine();


        manager.getBirthdays().forEach((k, v) -> {
            if (v.contains(name))
            {
                System.out.println(name + "'s " + k.countdown());
            }
        });
    }


    /**
     * Awaits new contact info, validates and adds it to the birthday manager.
     */
    private void addOption()
    {
        System.out.println(INPUT_MSG);
        String name = sc.nextLine();

        if (Validator.validateName(name) == 0)
        {
            System.out.println(MSGERR);
            name = sc.nextLine();
        }

        System.out.println("Please input birthday /in format dd.MM.YYYY/: ");

        String birthday = sc.nextLine();

        // If it's not the name validation err, it will be wrong input with the date.
        Birthday bd;
        try
        {
            bd = new Birthday(name, birthday);
            manager.addBirthday(bd);
        } catch (DateTimeParseException e)
        {
            System.out.println("Please type birthday in the correct format.");
            addOption();
        }
    }

}
