package entities;


import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 * @author Borimir Georgiev
 */
public class Birthday
{
    private static final DateTimeFormatter DATE_FORMAT_WITH_YEAR = DateTimeFormatter.ofPattern("d/M/yyyy");
    private static LocalDate today = LocalDate.now();

    private String person;
    private LocalDate birthday;

    public Birthday(String personName, String date)
    {
        this.person = personName;
        setBirthday(date);
    }

    public String getPerson()
    {
        return this.person;
    }

    public LocalDate getBirthday()
    {
        return this.birthday;
    }

    /**
     * Replaces the dots within the user input in order to be parsed correctly.
     *
     * @param date
     */
    private void setBirthday(String date)
    {
        date = date.replace('.', '/');
        this.birthday = LocalDate.parse(date, DATE_FORMAT_WITH_YEAR);
    }


    public String getAge()
    {
        Period p = getPeriod(birthday, today);

        return p.getYears() + " years, " + p.getMonths() +
                " months and " + p.getDays() +
                " days old.";
    }

    /**
     * Gets the month and day of birth, combines them with the current year in order to
     * create a valid LocalDate and check if birthday haven't passed already.
     * If it's passed, next birthday is next year - naturally we add 1 year to the variable.
     *
     * Then we calculate the period from today and construct a String with the coundown in months and days.
     *
     * @return countdown period as a String.
     */
    public String countdown()
    {
        MonthDay monthDayOfBirth = MonthDay.from(birthday);
        int year = today.getYear();
        LocalDate nextBirthday = monthDayOfBirth.atYear(year);

        if (nextBirthday.isBefore(today))
        {
            nextBirthday = nextBirthday.plusYears(1);
        }

        Period period = getPeriod(today, nextBirthday);

        int months = period.getMonths();
        int days = period.getDays();

        String message;

        // Just to put it grammatically correct. Adding the 's' if there's more than a month.
        if (months > 1)
        {
            message = "next birthday is after " + months + " months, " + days + " days.";
        } else {
            message = "next birthday is after " + months + " month, " + days + " days.";
        }

        return message;
    }


    private Period getPeriod(LocalDate date1, LocalDate date2)
    {
        return Period.between(date1, date2);
    }

    @Override
    public String toString()
    {
        return "Birthday: " + this.birthday;
    }
}
