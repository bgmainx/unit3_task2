package entities;

import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Constructs a birthday manager with a treemap within, which contains the birthdays
 * as keys and list of people born on that date, starting with the soonest birthday.
 *
 * @author Borimir Georgiev
 */
public class BirthdayManager
{
    private TreeMap<Birthday, List<String>> birthdays = new TreeMap<>(new DateComparator());

    public void addBirthday(Birthday bd)
    {
        if (!birthdays.containsKey(bd))
        {
            birthdays.put(bd, new ArrayList<>());
            birthdays.get(bd).add(bd.getPerson());
        } else
        {
            birthdays.get(bd).add(bd.getPerson());
        }
    }


    public SortedMap<Birthday, List<String>> getBirthdays()
    {
        return this.birthdays;
    }

    /**
     * We get the first key, because our map is already sorted by dates compared to
     * the current date.
     */
    public void getSoonestBirthday()
    {
        MonthDay soonest = MonthDay.from(birthdays.firstKey().getBirthday());
        DateTimeFormatter formatter  = DateTimeFormatter.ofPattern("dd/MMM");


        System.out.println("Soonest birthday is on " + formatter.format(soonest));
        System.out.println();
        System.out.println("People born on this day:");
        if (!birthdays.firstEntry().getValue().isEmpty())
        {
            birthdays.firstEntry().getValue().forEach(p -> System.out.println("- " + p));
        }
    }
}
