package entities;

/**
 * @author Borimir Georgiev
 */
public abstract class Validator
{
    public static int validateName(String name)
    {
        // name should contain only letters
        if (name.equals(" ") || !name.matches("[A-Z][a-zA-Z][^#&<>\\\"~;$^%{}*?\\d]{1,20}$"))
        {
            return 0;
        }

        return 1;
    }

}
