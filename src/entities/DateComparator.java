package entities;

import java.time.MonthDay;
import java.util.Comparator;


/**
 * @author Borimir Georgiev
 */
public class DateComparator implements Comparator<Birthday>
{

    @Override
    public int compare(Birthday o1, Birthday o2)
    {
        MonthDay monthDayOfBirth1 = MonthDay.from(o1.getBirthday());
        MonthDay monthDayOfBirth2 = MonthDay.from(o2.getBirthday());

        // If both dates are after now, sort them in reversing order
        if (monthDayOfBirth1.isAfter(MonthDay.now()) && monthDayOfBirth2.isAfter(MonthDay.now())) {
            return o1.getBirthday().compareTo(o2.getBirthday());
        }
        // If both dates are before now, sort them in normal order
        if (monthDayOfBirth1.isBefore(MonthDay.now()) && monthDayOfBirth2.isBefore(MonthDay.now())) {
            return o2.getBirthday().compareTo(o1.getBirthday());
        }
        return o1.getBirthday().compareTo(o2.getBirthday());

        // 13.01.2005
        // 10.10.1999
        // 15.01.1999
        // 13.08.1999

    }
}
