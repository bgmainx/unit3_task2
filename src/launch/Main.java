package launch;

import entities.Menu;

/**
 * @author Borimir Georgiev
 */
public class Main
{
    public static void main(String[] args)
    {
        App.run();
    }
}
